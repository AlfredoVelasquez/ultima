Rails.application.routes.draw do

  post "/products/new"

  root "stores#show" 

  resources :stores
  resources :products
  resources :admins

  namespace :api do
    namespace :v1 do
      resources :products
      end
    end
end
