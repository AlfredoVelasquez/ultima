class AdminsController < ApplicationController
  
  def index
    @admin = Product.all
  end
  
  def show
    @admin = Product.all
  end

  def new
    @admin = Product.new
  end

  def create 
    @admin = Product.new(product_params)
    if @admin.save
      redirect_to admin_path
    else
    render :new
   end
  end

  private
  
     def product_params
       params.require(:admin).permit(:name, :image, :description, :price, :quantity, :store_id)
     end
  
end
