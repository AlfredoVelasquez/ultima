class   StoresController < ApplicationController
    http_basic_authenticate_with name: "admin", password: "secret", only: "index"

    
    def index 
     @stores = Product.all
    end

    def show
    end
    
    def destroy
     Product.find(params[:id]).delete
     redirect_to stores_path, :alert => "Watch it, mister!"
    end
       
 end