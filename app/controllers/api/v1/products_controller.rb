class Api::V1::ProductsController < ApplicationController
    def index 
        @products = Product.all
        render :json => @products
       end
       
       def show
       end
       
       def edit
    
       end
       
       def update
       
       end
       
       def new
        @product = Product.new
       end
       
       def create 
        @product = Product.new(product_params)
        if @product.save
          redirect_to product_path
        else
        render :new
       end
      end
       
       def destroy
       Product.find(params[:id]).delete
       render :json => Product.all
       end
    
     
    
       private
    
       def product_params
         params.require(:person).permit(:name, :image, :description, :price, :quantity, :store_id)
       end
end
