class ProductSerializer < ActiveModel::Serializer
  attributes :id, :name, :image, :description, :price, :quantity, :store, :category

  belongs_to :store
  
  
    def store
      object.store.name
    end
  
    def category
      object.store.category
    end
end
