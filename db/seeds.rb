# This file should contain all the record creation needed to seed the database with its default values.
# The data can then be loaded with the rails db:seed command (or created alongside the database with db:setup).
#
# Examples:
#
#   movies = Movie.create([{ name: 'Star Wars' }, { name: 'Lord of the Rings' }])
#   Character.create(name: 'Luke', movie: movies.first)

Store.create(name: 'Hack Library', category: 'Librería y Papelería')
Admin.create(user_name: 'hackerman', password: '123321', store_id: 1)
Product.create(name: "Lapiz mongol 4k", description: "Lapiz mongol original mina ultra resistente para hacer dibujos HD y 4K.", image:"https://www.juansola.com/3646-large_default/lapiz-staedtler-noris-club-triplus-119-grueso.jpg", price: 250, quantity: 50, store_id: 1)
Product.create(name: "Borrador nata", description: "Un borrador  corriente marca nata original. borra todos tus errores, pero no los corrige.", image:"http://libreriacervantes.com.sv/wp-content/uploads/2016/03/borrador-dust-free.jpg", price: 1, quantity: 50, store_id: 1)
Product.create(name: "Pincel profesional", description: "Pincel profesional plano con bordes de oro e incrustaciones de diamantes.", image:"https://pincelhadasonline.es/3711-large_default/pincel-profesional-mascarilla-de-eurostil.jpg", price: 600.50, quantity: 50, store_id: 1)
Product.create(name: "Cuaderno de papel moneda", description: "Un cuaderno valioso para anotar informacion común", image:"https://www.officedepot.com.mx/medias/?context=bWFzdGVyfHJvb3R8MjkzNjR8aW1hZ2UvZ2lmfGg4MC9oODkvODkzNTEyNzgxMDA3OC5naWZ8ZTBhN2U3MjBmOTJmZDhlMGMxYzFkNGFlZTg2MmUxYjdkZWRhMWZhOTUxYWY5ZDc3OThkYzg2ZWQ3NTViYTY5MA", price: 245.99, quantity: 50, store_id: 1)
Product.create(name: "Sacapuntas de titanio", description: "el compañero perfecto para el Lapiz 4k", image:"https://www.officedepot.com.mx/medias/?context=bWFzdGVyfHJvb3R8Mzg1Mzd8aW1hZ2UvZ2lmfGhjZC9oYzgvODc5NjcxMjU5OTU4Mi5naWZ8YjUxY2FmNDE2OGIyM2IwODNiZWFkODY0MWYyMzYyZmYzNTMyNjVlZjYwYzg5OTlhMTBmYjFlZjJlNjFkNzg0MA", price: 95.00, quantity: 50, store_id: 1)