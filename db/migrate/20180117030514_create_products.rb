class CreateProducts < ActiveRecord::Migration[5.1]
  def change
    create_table :products do |t|
      t.string :name
      t.string :image
      t.string :description
      t.decimal :price
      t.integer :quantity
      t.references :store, foreign_key: true

      t.timestamps
    end
  end
end
